<?php

use App\Http\Controllers\dataController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [dataController::class, 'tampilan'] );

Route::get('/tambahdata', [dataController::class, 'tampilan']);

Route::post('/simpan', [dataController::class,'save']);

Route::get('/hapus/{id}',[dataController::class,'delete']);

Route::get('/edit/{id}',[dataController::class,'edit']);

Route::post('/update/{id}',[dataController::class,'update']);