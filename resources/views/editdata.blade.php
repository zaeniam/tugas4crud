@extends('welcome')
@section('container')
<h2 align="center"><u>EDIT DATA KARYAWAN</u></h2>
<section class="resume-section" id="tambahdata">
  @foreach ($karyawan as $krywn)
  <div class="container"> 
    <form action="/update/{{ $krywn->id }}" method="POST">
        {{ csrf_field() }}  
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Nama</label>
          <input type="name" class="form-control" name="nama_karyawan" value="{{ $krywn->nama_karyawan }}">
        </div>
        <div class="mb-3">
          <label for="exampleInputNumber" class="form-label">No Karyawan</label>
          <input type="number" class="form-control" name="no_karyawan" value="{{ $krywn->no_karyawan }}">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">No telepon</label>
            <input type="number" class="form-control" name="no_telp_karyawan" value="{{ $krywn->no_telp_karyawan }}">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Jabatan</label>
            <input type="Name" class="form-control" name="jabatan_karyawan" value="{{ $krywn->jabatan_karyawan }}">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Divisi</label>
            <input type="Name" class="form-control" name="divisi_karyawan" value="{{ $krywn->divisi_karyawan }}">
          </div>
        <button type="submit" class="btn btn-success">Simpan</button>
        <a href="/tambahdata"><button type="button" class="btn btn-danger ">Batal</button></a>
    </form>
  </div>  
  @endforeach
</section>
@endsection