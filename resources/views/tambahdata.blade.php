@extends('welcome')
@section('container')
<section class="resume-section" id="tambahdata">
<h2 align="center"><u>TAMBAH DATA KARYAWAN</u></h2>
<div class="container">
    <form action="/simpan" method="POST">
        {{ csrf_field() }}
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Nama</label>
          <input type="name" class="form-control" name="nama_karyawan">
        </div>
        <div class="mb-3">
          <label for="exampleInputNumber" class="form-label">No Karyawan</label>
          <input type="number" class="form-control" name="no_karyawan">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">No telepon</label>
            <input type="number" class="form-control" name="no_telp_karyawan">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Jabatan</label>
            <input type="Name" class="form-control" name="jabatan_karyawan">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Divisi</label>
            <input type="Name" class="form-control" name="divisi_karyawan">
          </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  </section><br>
  
  <div class="container">
  <?php $i=1;?>
  <div class="align-self-center">
    <table class="table table-warning table-bordered table-striped">
        <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama</th>
              <th scope="col">No Karyawan</th>
              <th scope="col">No Telepon</th>
              <th scope="col">Jabatan</th>
              <th scope="col">Divisi</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
      @foreach ($karyawan as $krywn)
        <tbody>
          <tr>
            <td><?php echo($i); $i++; ?></td>
            <th scope="col">{{ $krywn->nama_karyawan }}</th>
            <th scope="col">{{ $krywn->no_karyawan }}</th>
            <th scope="col">{{ $krywn->no_telp_karyawan }}</th>
            <th scope="col">{{ $krywn->jabatan_karyawan }}</th>
            <th scope="col">{{ $krywn->divisi_karyawan }}</th>
            <th scope="col">
              <a href="/edit/{{ $krywn->id }}"><button type="button" class="btn btn-warning ">Edit</button></a>
              <a href="/hapus/{{ $krywn->id }}"><button type="button" class="btn btn-danger ">Delete</button></a>
          </tr>
        </tbody>
      @endforeach
    </table>
  </div>
  </div>
@endsection