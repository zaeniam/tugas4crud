<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class dataController extends Controller
{
    public function tampilan ()
    {
        $karyawan = DB::table('karyawan')->get();
        return view('tambahdata',['karyawan'=>$karyawan]);
    }

    public function save (Request $req)
    {
        $nama = $req->nama_karyawan;
        $nokaryawan = $req->no_karyawan;
        $notelepon = $req->no_telp_karyawan;
        $jabatan = $req->jabatan_karyawan;
        $divisi = $req->divisi_karyawan;

        DB::table('karyawan')->insert([
            'nama_karyawan'=>$nama,
            'no_karyawan'=>$nokaryawan, 
            'no_telp_karyawan'=>$notelepon,
            'jabatan_karyawan'=>$jabatan,
            'divisi_karyawan'=>$divisi]
        );

        return redirect('/tambahdata');
    }

    public function update (Request $req,Int $id)
    {
        DB::table('karyawan')->where('id', $req->id)->update([
        'nama_karyawan' => $req->nama_karyawan,
        'no_karyawan' => $req->no_karyawan,
        'no_telp_karyawan' => $req->no_telp_karyawan,
        'jabatan_karyawan' => $req->jabatan_karyawan,
        'divisi_karyawan' => $req->divisi_karyawan,
        ]);

        return redirect('/tambahdata');
    }

    public function edit($id)
    {
        $karyawan = DB::table('karyawan')->where('id', $id)->get();
        return view('editdata', ['karyawan' => $karyawan]);
    }

    public function delete($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/tambahdata');
    }
}